#include <iostream>
#include <string>
using namespace std;

const int MAX = 1000;

//联系人结构体
typedef struct person
{
    string m_name;          //姓名
    int m_sex;              //性别 1 男，0 女
    int m_age;              //年龄
    string m_phone;         //电话
    string m_addr;          //住址
}person_t;

//通讯录结构体
typedef struct addressbooks
{
    person_t personArray[MAX];  //通讯录中保存的联系人数组
    int m_size;                 //通讯录中人员个数
}addressbooks_t;

void showMenu()
{
    cout << "************************" << endl;
    cout << "***** 1.添加联系人 *****" << endl;
    cout << "***** 2.显示联系人 *****" << endl;
    cout << "***** 3.删除联系人 *****" << endl;
    cout << "***** 4.查找联系人 *****" << endl;
    cout << "***** 5.修改联系人 *****" << endl;
    cout << "***** 6.清空联系人 *****" << endl;
    cout << "***** 0.退出通讯录 *****" << endl;
    cout << "************************" << endl;
}

void addPerson(addressbooks_t *abs)
{
    //判断通讯录是否满了，如果满了就不再添加
    if(abs->m_size == MAX)
    {
        cout  << "通讯录已满，无法添加！" << endl;
        return ;
    }
    else
    {
        //添加具体联系人
        cout << "请输入姓名： ";
        cin >> abs->personArray[abs->m_size].m_name;

        int sex;
        while(true)
        {
            cout << "请输入性别（1男，0女）： ";
            cin >> sex;
            if(sex == 1 || sex == 0)
            {
                abs->personArray[abs->m_size].m_sex = sex;
                break;
            }
            else
            {
                cout << "输入有误，请重新输入！" << endl;
            }
        }

        cout << "请输入年龄： ";
        cin >> abs->personArray[abs->m_size].m_age;

        cout << "请输入电话： ";
        cin >> abs->personArray[abs->m_size].m_phone;

        cout << "请输入地址： ";
        cin >> abs->personArray[abs->m_size].m_addr;

        //更新通讯录人数
        ++ abs->m_size;

        cout << "添加成功！" << endl;
        system("pause");
    }
}

void displayPerson(addressbooks_t *abs)
{
    if(abs->m_size == 0)
    {
        cout << "记录为空!" << endl;
    }
    else
    {
        cout << "姓名\t" << "年龄\t" << "性别\t" << "电话\t\t" << "地址\t\t" << endl;
        for(int i = 0; i < abs->m_size; ++i)
        {
            cout << abs->personArray[i].m_name << "\t";
            cout << abs->personArray[i].m_age << "\t";
            cout << (abs->personArray[i].m_sex == 1 ? "男" : "女") << "\t";
            cout << abs->personArray[i].m_phone << "\t";
            cout << abs->personArray[i].m_addr << "\t";
            cout << endl;
        }
    }
    system("pause");
}

//按姓名查找用户，如果查找到返回联系人在数组中的下标，未查找到返回-1
int findByName(addressbooks_t *abs,string name)
{
    for(int i = 0; i < abs->m_size; ++i)
    {
        if(name == abs->personArray[i].m_name)
        {
            return i;
        }
    }
    return -1;
}

void deletePerson(addressbooks_t *abs)
{
    string name;
    cout << "请输入要删除联的系人姓名：";
    cin >> name;
    int index = findByName(abs,name);
    if(index == -1)
    {
        cout << "通讯录中未查到" << name << endl;
    }
    else
    {
        for(int i = index; i < abs->m_size; ++i)
        {
            abs->personArray[i] = abs->personArray[i+1];
        }

        --abs->m_size;
        cout << "删除成功" << endl;
    }
    system("pause");
}

void findPerson(addressbooks_t *abs)
{
    string name;
    cout << "请输入要查找的联系人姓名：";
    cin >> name;
    int index = findByName(abs,name);
    if(index == -1)
    {
        cout << "通讯录中未查到" << name << endl;
    }
    else
    {
        cout << "姓名\t" << "年龄\t" << "性别\t" << "电话\t\t" << "地址\t\t" << endl;
        cout << abs->personArray[index].m_name << "\t";
        cout << abs->personArray[index].m_age << "\t";
        cout << (abs->personArray[index].m_sex == 1 ? "男" : "女") << "\t";
        cout << abs->personArray[index].m_phone << "\t";
        cout << abs->personArray[index].m_addr << "\t";
        cout << endl;
    }
    system("pause");
}

void updatePerson(addressbooks_t *abs)
{
    string name;
    cout << "请输入要修改的联系人姓名：";
    cin >> name;
    int index = findByName(abs,name);
    if(index == -1)
    {
        cout << "通讯录中未查到" << name << endl;
    }
    else
    {
        //添加具体联系人
        cout << "请输入姓名： ";
        cin >> abs->personArray[index].m_name;

        int sex;
        while(true)
        {
            cout << "请输入性别（1男，0女）： ";
            cin >> sex;
            if(sex == 1 || sex == 0)
            {
                abs->personArray[index].m_sex = sex;
                break;
            }
            else
            {
                cout << "输入有误，请重新输入！" << endl;
            }
        }

        cout << "请输入年龄： ";
        cin >> abs->personArray[index].m_age;

        cout << "请输入电话： ";
        cin >> abs->personArray[index].m_phone;

        cout << "请输入地址： ";
        cin >> abs->personArray[index].m_addr;

        cout << "修改成功！" << endl;
    }
    system("pause");
}

void clearPerson(addressbooks_t *abs)
{
    abs->m_size = 0;
    cout << "清空成功!" << endl;
    system("pause");
}

int main()
{
    //创建一个结构体变量
    addressbooks_t abs;
    //初始化通讯录中人员个数
    abs.m_size = 0;

    int select = 0;                 //创建用户选择输入的变量
    while(true)
    {
        system("cls");              //清空屏幕
        showMenu();                 //菜单调用
        cout << "请输入您的选择: ";
        cin >> select;
        switch (select)
        {
        case 0:                      //0.退出通讯录
            cout << "欢迎下次使用" << endl;
            return 0;
            break;
        case 1:                      //1.添加联系人
            addPerson(&abs);
            break;
        case 2:                      //2.显示联系人
            displayPerson(&abs);
            break;
        case 3:                      //3.删除联系人
            deletePerson(&abs);
            break;
        case 4:                      //4.查找联系人
            findPerson(&abs);
            break;
        case 5:                      //5.修改联系人
            updatePerson(&abs);
            break;
        case 6:                      //6.清空联系人
            clearPerson(&abs);
            break;
        default:
            break;
        }
    }

    return 0;
}

